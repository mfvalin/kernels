/* Library of useful routines for C and FORTRAN programming
 * Copyright (C) 2019  Division de Recherche en Prevision Numerique
 *                          Environnement Canada
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation,
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>

static uint64_t time0 = 0;

#if !defined(NO_FORTRAN_INTERFACE)
// popular Fortran manglings
#pragma weak cpu_clock_cycles__=cpu_real_time_ticks
#pragma weak cpu_clock_cycles_=cpu_real_time_ticks
uint64_t cpu_clock_cycles__(void);
uint64_t cpu_clock_cycles_(void);

#pragma weak cpu_real_time_ticks__=cpu_real_time_ticks
#pragma weak cpu_real_time_ticks_=cpu_real_time_ticks
uint64_t cpu_real_time_ticks__(void);
uint64_t cpu_real_time_ticks_(void);

#pragma weak cpu_real_time_clock__=cpu_real_time_clock
#pragma weak cpu_real_time_clock_=cpu_real_time_clock
uint64_t cpu_real_time_clock__(void);
uint64_t cpu_real_time_clock_(void);

#pragma weak cpu_ticks_and_id__=cpu_ticks_and_id
uint64_t cpu_ticks_and_id__(int *socket, int *core);
#pragma weak cpu_ticks_and_id_=cpu_ticks_and_id
uint64_t cpu_ticks_and_id_(int *socket, int *core);

#endif

uint64_t cpu_real_time_ticks(void);

uint64_t cpu_real_time_clock(void) {
  struct timeval tv;
  gettimeofday(&tv,NULL);
  time0 = tv.tv_sec;
  time0 *= 1000000 ;   // seconds to microseconds
  time0 += tv.tv_usec; // add microseconds
  return time0;
}
static uint64_t timerfreq = 0;

void init_cpu_timers(){
#if defined(__x86_64__)
  uint32_t eax, ebx, ecx, edx, i;
  uint32_t vstring[12];
  char *cstring = (char*) vstring;
  double freq;
  uint64_t t0, t1, ta, tb;

  // get frequency from the identification string (only reliable source)
  eax = 0x80000002 ; ecx = 0;
  __asm__ ( "cpuid" : "+b" (ebx), "+a" (eax), "+c" (ecx), "=d" (edx) );
  vstring[ 0] = eax ; vstring[ 1] = ebx ; vstring[ 2] = ecx ; vstring[ 3] = edx ;

  eax = 0x80000003 ; ecx = 0;
  __asm__ ( "cpuid" : "+b" (ebx), "+a" (eax), "+c" (ecx), "=d" (edx) );
  vstring[ 4] = eax ; vstring[ 5] = ebx ; vstring[ 6] = ecx ; vstring[ 7] = edx ;

  eax = 0x80000004 ; ecx = 0;
  __asm__ ( "cpuid" : "+b" (ebx), "+a" (eax), "+c" (ecx), "=d" (edx) );
  vstring[ 8] = eax ; vstring[ 9] = ebx ; vstring[10] = ecx ; vstring[11] = edx ;

  i=0 ; while(cstring[i]) i++; while(cstring[i] != ' ') i-- ; i++;
  sscanf((const char *__restrict__)(cstring+i),"%lf",&freq);
  timerfreq = freq * 1000000000.0;
  if(timerfreq == 0) {  // processor id string useless to get processor base frequency
    ta = cpu_real_time_clock();
    t0 = cpu_real_time_ticks();
    usleep(10000);
    t1 = cpu_real_time_ticks();
    tb = cpu_real_time_clock();
    freq = (t1-t0) ; freq /= (tb-ta) ;
    timerfreq = (freq/10) + .5;         // round to nearest .01 GHz
    timerfreq = timerfreq * 1000;  // convert to Hz
  }
#endif
#if defined(__aarch64__)
  asm volatile("mrs %0, cntfrq_el0" : "=r"(timerfreq));
#endif
}

uint64_t get_cpu_timer_frequency(){
  if(timerfreq == 0) init_cpu_timers();
  return timerfreq;
}

uint64_t cpu_real_time_ticks_ooo(void) {
#if defined(__x86_64__)
  uint32_t lo, hi;   // "out of order" version
  __asm__ volatile ("rdtsc"
      : /* outputs   */ "=a" (lo), "=d" (hi)
      : /* no inputs */
      : /* clobbers  */ "%rcx");
  return time0 = (uint64_t)lo | (((uint64_t)hi) << 32) ;
#endif
#if defined(__aarch64__)
  asm volatile ("mrs %0, cntvct_el0" : "=r" (time0));
  return time0;
#endif
#if !defined(__x86_64__) && !defined(__aarch64__)
  time0 = cpu_real_time_clock();  // a tick will be a microsecond in this case
  return time0;
#endif
}

uint64_t cpu_real_time_ticks(void) {
#if defined(__x86_64__)
  uint32_t lo, hi;   // "in order" version
  __asm__ volatile ("rdtscp"
      : /* outputs   */ "=a" (lo), "=d" (hi)
      : /* no inputs */
      : /* clobbers  */ "%rcx");
  return time0 = (uint64_t)lo | (((uint64_t)hi) << 32) ;
#endif
#if defined(__aarch64__)
  asm volatile ("isb; mrs %0, cntvct_el0" : "=r" (time0));
  return time0;
#endif
#if !defined(__x86_64__) && !defined(__aarch64__)
  time0 = cpu_real_time_clock();  // a tick will be a microsecond in this case
  return time0;
#endif
}

uint64_t cpu_ticks_and_id(int *socket, int *core){  // get tsc/socket/core
#if defined(__x86_64__) &&  defined(__linux__)
   uint32_t lo, hi, c;
   // rdtscp instruction
   // EDX:EAX contain TimeStampCounter
   // ECX contains IA32_TSC_AUX[31:0] (MSR_TSC_AUX value set by OS, lower 32 bits contain socket+core)
   __asm__ volatile("rdtscp" : "=a" (lo), "=d" (hi), "=c" (c));
    *socket = (c>>12) & 0xFFF;
    *core   =  c      & 0xFFF;

   return time0 = (uint64_t)lo | (((uint64_t)hi) << 32) ;
#else  
  time0   = cpu_real_time_clock();  // a tick will be a microsecond in this case
  *socket = -1;                     // no socket/core info available
  *core   = -1;
  return time0;
#endif
}


#if defined(SELF_TEST)
#include <stdio.h>
#include <unistd.h>
int main(){
  uint64_t ts[100], t0, t1, ta, tb;
  int i,j;
  double avg;

//   init_cpu_timers();
  fprintf(stdout,"Timer frequency is %ld Hz\n",get_cpu_timer_frequency());
  for(i=0;i<100;i++) ts[i] = cpu_real_time_ticks();
  ta = cpu_real_time_clock();
  t0 = cpu_real_time_ticks();
  usleep(10000);                // sleep for about 10 milliseconds
  t1 = cpu_real_time_ticks();
  tb = cpu_real_time_clock();
  fprintf(stdout,"There are %ld ticks in a microsecond\n",(t1-t0)/(tb-ta));
//   avg = (t1-t0) ; avg /=(tb-ta) ; te = (avg/10) + .5; te = te * 10000000;
//   fprintf(stdout,"Estimated Timer frequency is %ld Hz\n",te);
  fprintf(stdout,"sample overhead : ") ; for(i=1;i<100;i+=4) fprintf(stdout," %ld",ts[i]-ts[i-1]) ; fprintf(stdout," 'ticks'\n");
//   avg = ts[75] - ts[26]; avg = avg / 50.0 ; avg = avg / (t1-t0) ; avg = avg * 10000000. ;
  avg = ts[75] - ts[26]; avg = avg / 50.0 ; avg = avg / (get_cpu_timer_frequency()) ; avg = avg * 1000000000. ;
  fprintf(stdout,"average overhead per call (50 calls) ~ %6.2f ns\n\n\n",avg);

  for(i=0;i<100;i++) ts[i] = cpu_real_time_ticks_ooo();
  fprintf(stdout,"sample overhead (fast) : ") ; for(i=1;i<100;i+=4) fprintf(stdout," %ld",ts[i]-ts[i-1]) ; fprintf(stdout," 'ticks'\n");
  avg = ts[75] - ts[26]; avg = avg / 50.0 ; avg = avg / (get_cpu_timer_frequency()) ; avg = avg * 1000000000. ;
  fprintf(stdout,"average overhead per call (50 calls) ~ %6.2f ns\n\n",avg);

  for(i=0;i<100;i++) {
    ts[i] = cpu_real_time_clock();
    for(j=1;j<1000;j++) t0 = cpu_real_time_clock();
  }
  t0 = cpu_real_time_clock();
  usleep(10000);
  t1 = cpu_real_time_clock();
  fprintf(stdout,"There are %ld microseconds in a second\n",(t1-t0)*100);
  fprintf(stdout,"overhead per 1000 calls : ") ; 
  for(i=1;i<100;i+=4) fprintf(stdout," %ld",(ts[i]-ts[i-1])) ; 
  fprintf(stdout," 'microseconds'\n");
  avg = ts[75] - ts[26]; avg = avg / 50.0 ;
  avg = avg / 1000; // timing blocks of 1000 calls
  fprintf(stdout,"average overhead per call (50000 calls) ~ %6.2f ns\n",avg*1000.);

  ta = cpu_ticks_and_id(&i, &j);
  fprintf(stdout,"Socket = %d, Core = %d\n",i,j);
  return 0;
}
#endif
