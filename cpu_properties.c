/* Library of hopefully useful routines for C and FORTRAN
 * Copyright (C) 2019  Recherche en Prevision Numerique
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation,
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#if defined(MUST_NEVER_BE_TRUE)
//****P* librkl/cpu_properties
// Synopsis
//  set of X86 utilities for cpu type and properties determination
// EXAMPLES
Fortran example:

program test_feature
  use ISO_C_BINDING
  implicit none
#define IN_FORTRAN_CODE
#include <cpu_properties.h>
  real(C_DOUBLE) :: s1, s2
  integer(C_INT64_T) :: t1, t2
  integer :: status
  if(cpu_has_feature(FLAG_AVX2) .ne. 0) print *,'AVX2'
  if(cpu_has_feature(FLAG_AVX) .ne. 0) print *,'AVX'
  if(cpu_has_feature(FLAG_FMA) .ne. 0) print *,'FMA'
  if(cpu_has_feature(FLAG_BMI) .ne. 0) print *,'BMI'
  if(cpu_has_feature(FLAG_SSE4) .ne. 0) print *,'SSE4'
  if(cpu_has_feature(FLAG_SSE3) .ne. 0) print *,'SSE3'
  if(cpu_has_feature(FLAG_SSE2) .ne. 0) print *,'SSE2'
  status = get_fp_status_ctl()
  print 100,' FP status = ',status
100 format(A,Z8)
  if(iand(FP_STATUS_PE,status) .ne. 0) print *,"Precision ON"
  if(iand(FP_STATUS_UE,status) .ne. 0) print *,"Underflow ON"
  if(iand(FP_STATUS_OE,status) .ne. 0) print *,"Overflow ON"
  if(iand(FP_STATUS_ZE,status) .ne. 0) print *,"Zero divide ON"
  print *,'CPU number =',get_cpu_number()
  stop
end

result of Fortran program execution on i3-6100 CPU @ 3.70GHz :

 AVX2
 AVX
 FMA
 BMI
 SSE4
 SSE3
 SSE2
 FP status =     1FA0
 Precision ON

C example:

#include <stdint.h>
#include <stdio.h>
#include <cpu_properties.h>
int main(int argc, char** argv)
{
 uint64_t t1, t2;
 double s1, s2;
 int status;
 printf("CPU speed: %lu Hz\n",get_cpu_freq());
 printf("FLAGS: ");
 if(cpu_has_feature(FLAG_SSE))  printf(" SSE");
 if(cpu_has_feature(FLAG_SSE2)) printf(" SSE2");
 if(cpu_has_feature(FLAG_SSE3)) printf(" SSE3");
 if(cpu_has_feature(FLAG_SSE4)) printf(" SSE4");
 if(cpu_has_feature(FLAG_AVX))  printf(" AVX");
 if(cpu_has_feature(FLAG_FMA))  printf(" FMA");
 if(cpu_has_feature(FLAG_AVX2)) printf(" AVX2");
 printf("\n");
 status = get_fp_status_ctl();
 printf("FPU status = %8.8x\n",status);
 printf("Precision status %s\n",status & FP_STATUS_PE ? "ON" : "OFF");
 printf("Underflow status %s\n",status & FP_STATUS_UE ? "ON" : "OFF");
 printf("Overflow status %s\n",status & FP_STATUS_OE ? "ON" : "OFF");
 printf("Zero divide status %s\n",status & FP_STATUS_ZE ? "ON" : "OFF");
 return(0);
}

result of C program execution on i3-6100 CPU @ 3.70GHz :

CPU speed: 3700000000 Hz
FLAGS:  SSE SSE2 SSE3 SSE4 AVX FMA AVX2
FPU status = 00001fa0
Precision status ON
Underflow status OFF
Overflow status OFF
Zero divide status OFF
//****
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <cpu_properties.h>

#define MAX_APIC 512
static short *APICids = NULL;
static int Maxcpus = -1;
static int Maxapicid = -1;

#define HAS_FLAG(flag,flagword) (flag & flagword)
#define X86_FLAG(flag) (flag & ProcessorCapabilities)

static int ProcessorCapabilities = 0 ;  /* by default, no capabilities are indicated as available */
static uint32_t vstring[12];            /* version string */
static unsigned char *cstring = (unsigned char *)vstring;  /* pointer to version string */
static uint64_t hz=0;
static double cycle=0.0;
static int ncores=0;
static int threadpercore=0;

// make apicid table from /proc/cpuinfo and use it to build apicid -> cpu number table
// linux OS only, useful only on X86-64
static int make_apicid_table(void){
#if defined(__linux__)
  char line[4096];
  short apics[MAX_APIC];
  char *ptr;
  int apicid, napics;
  int i;
  int lines = 0;
  int maxapicid = -1;
  int cpunumber = -1;
  FILE *fd;

  if(APICids != NULL){ // already initialized
    return Maxapicid;
  }
  fd=fopen("/proc/cpuinfo","r");
  for(i=0 ; i<MAX_APIC ; i++) apics[i] = -1;
  if(fd == NULL) return 0;
  ptr = fgets(line,sizeof(line),fd);
  while(ptr != NULL) {
    lines++;
    ptr = fgets(line,sizeof(line),fd);
    if(strncmp(line,"apicid",6) == 0) {
      cpunumber++;
      while( *ptr != ':') ptr++; ptr++;
      apicid = atoi(ptr);
      maxapicid = (maxapicid < apicid) ? apicid : maxapicid;
      if(apicid < MAX_APIC) apics[apicid] = cpunumber;   // associate apic id to cpu number
    }
  }
  fclose(fd);
  napics = maxapicid + 1 ;
  APICids = (short *)malloc(napics*sizeof(short));
  for(i=0 ; i<=maxapicid ; i++) APICids[i] = apics[i];
  Maxcpus = cpunumber+1;
  Maxapicid = maxapicid;
  return maxapicid;
#else
  return -1;
#endif
}

static void X86_cpuid(uint32_t eax, uint32_t ecx, uint32_t* regs)  /* interface to x86 cpuid instruction */
{
#if defined(__x86_64__) || defined( __i386__ )
    uint32_t ebx, edx;
# if defined( __i386__ ) && defined ( __PIC__ )
     /* PIC under 32-bit EBX must not be clobbered */
    __asm__ ( "movl %%ebx, %%edi \n\t cpuid \n\t xchgl %%ebx, %%edi" : "=D" (ebx),
# else
   ebx = 0;
    __asm__ ( "cpuid" : "+b" (ebx),
# endif
              "+a" (eax), "+c" (ecx), "=d" (edx) );
    regs[0] = eax; regs[1] = ebx; regs[2] = ecx; regs[3] = edx;
#else
    regs[0] = 0; regs[1] = 0; regs[2] = 0; regs[3] = 0;
#endif
}     

static void get_cpu_capabilities()
{
#if defined(__x86_64__)
  uint32_t regs[4];
  uint32_t j;
  float freq;

  X86_cpuid( 1, 0, regs );  /* get CPU capabilities EAX=1, ECX=0 */
  j = regs[0];
  if((1 <<  0) & regs[2]) ProcessorCapabilities |= FLAG_SSE3 ;  /* SSE3   ECX bit  0 */
  if((1 << 12) & regs[2]) ProcessorCapabilities |= FLAG_FMA ;   /* FMA    ECX bit 12 */
  if((1 << 20) & regs[2]) ProcessorCapabilities |= FLAG_SSE4 ;  /* SSE4.2 ECX bit 20 */
  if((1 << 28) & regs[2]) ProcessorCapabilities |= FLAG_AVX ;   /* AVX    ECX bit 28 */
  if((1 << 25) & regs[3]) ProcessorCapabilities |= FLAG_SSE ;   /* SSE    EDX bit 25 */
  if((1 << 26) & regs[3]) ProcessorCapabilities |= FLAG_SSE2 ;  /* SSE2   EDX bit 26 */

  X86_cpuid( 0x80000002, 0, regs );  /* version string (3 calls) */
  for(j=0 ; j<4 ; j++){
    vstring[j] = regs[j];
  }
  X86_cpuid( 0x80000003, 0, regs );
  for(j=0 ; j<4 ; j++){
    vstring[j+4] = regs[j];
  }
  X86_cpuid( 0x80000004, 0, regs );
  for(j=0 ; j<4 ; j++){
    vstring[j+8] = regs[j];
  }
  j = 0 ;
  while(cstring[j++]) ;
  j = j - 3;
  while(cstring[j] != ' ') j--;
  j++;
  sscanf((const char *__restrict__)(cstring+j),"%f",&freq);
  hz = freq*1000.0 + .5;  // MHz
  hz = hz * 1000000;      // Hz
  cycle = hz;
  cycle = 1.0 / cycle;    // seconds

  X86_cpuid( 0x0B, 0, regs ); threadpercore = regs[1] & 0xFFFF;
  if(threadpercore < 1) threadpercore = 1;
//   printf("0x0B, 0,EBX=%8.8x, ECX=%8.8x, eDX=%8.8x \n",regs[1],regs[2],regs[3]);
  X86_cpuid( 0x0B, 1, regs ); ncores = regs[1] & 0xFFFF; ncores /= threadpercore;
  if(ncores < 1) ncores = 1;
//   printf("0x0B, 1,EBX=%8.8x, ECX=%8.8x, eDX=%8.8x \n",regs[1],regs[2],regs[3]);
  X86_cpuid( 0x0B, 2, regs );
//   printf("0x0B, 2,EBX=%8.8x, ECX=%8.8x, eDX=%8.8x \n",regs[1],regs[2],regs[3]);

  if((ProcessorCapabilities & FLAG_FMA) == 0) return ; /* if FMA flag not present, AVX2 will not be */

  X86_cpuid( 7, 0, regs );    /* get more CPU capabilities EAX=7, ECX=0 */
  if((1 << 5) & regs[1]) ProcessorCapabilities |= FLAG_AVX2 ;   /* AVX2  EBX bit 5 */
  if((1 << 8) & regs[1]) ProcessorCapabilities |= FLAG_BMI  ;   /* BMI2  EBX bit 8 needed to set our BMI flag */

  j = make_apicid_table();
#else
  cstring = "Unknown processor";
#endif
}

static int CPU_from_apicid(int apicid){
#if defined(__x86_64__)
  if(ProcessorCapabilities == 0) get_cpu_capabilities();
  if(apicid < 0 || apicid > Maxapicid) return(-1);
  return(APICids[apicid]);
#else
  return(-1);
#endif  
}

/****f* librkl/get_cpu_freq
 * FUNCTION
 *  get the CPU nominal frequency
 * Synopsis
 *  C:
 *   uint64_t get_cpu_freq();
 *
 *  Fortran:
 *   interface
 *     function get_cpu_freq() result(freq)
 *       import C_INT64_T
 *       integer(C_INT64_T) :: freq
 *     end function get_cpu_freq
 *   end interface
 * AUTHOR
 *  M.Valin Recherche en Prevision Numerique 2016
 * ARGUMENTS
    none
 * RESULT
 *  the CPU nominal frequency in Hertz (64 bit unsigned integer) 
 *  (nonzero on X86 family cpus only)
 * 
*****
*/
#pragma weak get_cpu_freq__=get_cpu_freq
#pragma weak get_cpu_freq_=get_cpu_freq
uint64_t get_cpu_freq__(void);
uint64_t get_cpu_freq_(void);
uint64_t get_cpu_freq(void)
{
  if(ProcessorCapabilities == 0) get_cpu_capabilities();
  return( hz );
}

/****f* librkl/cpu_has_feature
 * FUNCTION
 *   determine whether certain capabilities are present on the current CPU
 *   e.g. AVX/AVX2/SSE2/FMA ...
 * Synopsis
 *  C:
 *   int cpu_has_feature(int feature);
 *
 *  Fortran:
 *   interface
 *     function cpu_has_feature(feature) result(status)
 *       import C_INT
 *       integer(C_INT), intent(IN), value :: feature
 *       integer(C_INT) :: status
 *     end function cpu_has_feature
 *   end interface
 *
 *  #include <cpu_type.h> 
 *  is usable by C and Fortran programs alike
 *
 *  Fortran programs MUST use
 *  #define IN_FORTRAN_CODE
 *  before including cpu_type.h
 * AUTHOR
 *  M.Valin Recherche en Prevision Numerique 2016
 * ARGUMENTS
    feature : feature symbol from cpu_type (#include <cpu_type.h>)
 * RESULT
 *  1 is feature is supported by CPU, 0 otherwise
*****
*/
#pragma weak cpu_has_feature__=cpu_has_feature
#pragma weak cpu_has_feature_=cpu_has_feature
int cpu_has_feature__(int flag);
int cpu_has_feature_(int flag);
int cpu_has_feature(int flag)
{
  if(ProcessorCapabilities == 0) get_cpu_capabilities();
  return( X86_FLAG(flag) );
}

/****f* librkl/get_cpu_hyperthreads
 * FUNCTION
 *   get the number of hyperthreads of this CPU
 *  Synopsis
 * C:
 *   int get_cpu_hyperthreads();
 *
 * Fortran:
 *   interface
 *     function get_cpu_hyperthreads() result(nhyperthreads)
 *       import C_INT
 *       integer(C_INT) :: nhyperthreads
 *     end function get_cpu_hyperthreads
 *   end interface
 * AUTHOR
 *  M.Valin Recherche en Prevision Numerique 2016
 * ARGUMENTS
    none
 * RESULT
 *  the number of hyperthreads of this CPU (not 1 on X86 family cpus only)
*****
*/
#pragma weak get_cpu_hyperthreads__=get_cpu_hyperthreads
#pragma weak get_cpu_hyperthreads_=get_cpu_hyperthreads
int get_cpu_hyperthreads__();
int get_cpu_hyperthreads_();
int get_cpu_hyperthreads()  /* Intel CPUs only */
{
  if(ProcessorCapabilities == 0) get_cpu_capabilities();
  return( threadpercore );
}

/****f* librkl/get_cpu_cores
 * FUNCTION
 *   get the number of cores of this CPU
 *  Synopsis
 * C:
 *   int get_cpu_cores();
 *
 * Fortran:
 *   interface
 *     function get_cpu_cores() result(ncores)
 *       import C_INT
 *       integer(C_INT) :: ncores
 *     end function get_cpu_cores
 *   end interface
 * AUTHOR
 *  M.Valin Recherche en Prevision Numerique 2016
 * ARGUMENTS
    none
 * RESULT
 *  the number of cores of this CPU (not one on X86 family cpus only)
*****
*/
#pragma weak get_cpu_cores__=get_cpu_cores
#pragma weak get_cpu_cores_=get_cpu_cores
int get_cpu_cores__();
int get_cpu_cores_();
int get_cpu_cores()  /* Intel CPUs only */
{
  if(ProcessorCapabilities == 0) get_cpu_capabilities();
  return( ncores );
}

/****f* librkl/get_cpu_number
 * FUNCTION
 *   get the processor number of this CPU in the node
 *  Synopsis
 * C:
 *   int get_cpu_number();
 *
 * Fortran:
 *   interface
 *     function get_cpu_number() result(id)
 *       import C_INT
 *       integer(C_INT) :: id
 *     end function get_cpu_number
 *   end interface
 * AUTHOR
 *  M.Valin Recherche en Prevision Numerique 2016
 * ARGUMENTS
    none
 * RESULT
 *  the APIC id of the current cpu (nozero on X86 family cpus only)
 * 
 *  to be correlated with /proc/cpuinfo to find actual CPU number
*****
*/
#pragma weak get_cpu_number__=get_cpu_number
#pragma weak get_cpu_number_=get_cpu_number
int get_cpu_number__();
int get_cpu_number_();
int get_cpu_number()  /* Intel CPUs only */
{
#if defined(__x86_64__)
  uint32_t regs[4];
  X86_cpuid( 0x0B, 0, regs );
  return( CPU_from_apicid(regs[3]) );  /* x2APIC id from EDX  */
#else
  return -1;                           // info not available
#endif
}

/****f* librkl/get_fp_status_ctl
 * FUNCTION
 *   get floating point status word
 *  Synopsis
 * C:
 *   int get_fp_status_ctl();
 *
 * Fortran:
 *   interface
 *     function get_fp_status_ctl() result(id)
 *       import C_INT
 *       integer(C_INT) :: id
 *     end function get_fp_status_ctl
 *   end interface
 * AUTHOR
 *  M.Valin Recherche en Prevision Numerique 2016
 * ARGUMENTS
    none
 * RESULT
 *  the floating point status word (nozero on X86 family cpus only)
*****
*/
#pragma weak get_fp_status_ctl__= get_fp_status_ctl
#pragma weak get_fp_status_ctl_= get_fp_status_ctl
int get_fp_status_ctl__(void);
int get_fp_status_ctl_(void);
int get_fp_status_ctl(void) {
  int fpstat = 0;
#if defined(__i386__) || defined(__x86_64__)
  __asm__ volatile ("stmxcsr %0" : "=m" (fpstat));
#endif
  return (fpstat);
}

/****f* librkl/set_fp_status_ctl
 * FUNCTION
 *   set the floating point status and control word
 *  Synopsis
 * C:
 *   void set_fp_status_ctl(int id);
 *
 * Fortran:
 *   interface
 *     subroutine set_fp_status_ctl(id)
 *       import C_INT
 *       integer(C_INT), intent(IN), value :: id
 *     end subroutine set_fp_status_ctl
 *   end interface
 * AUTHOR
 *  M.Valin Recherche en Prevision Numerique 2016
 * ARGUMENTS
    id : integer value to store into the floating point status and control word
         (X86 family cpus only)
*****
*/
#pragma weak set_fp_status_ctl__=set_fp_status_ctl
#pragma weak set_fp_status_ctl_=set_fp_status_ctl
void set_fp_status_ctl__(int fpstat_in);
void set_fp_status_ctl_(int fpstat_in);
void set_fp_status_ctl(int fpstat_in) {
  int fpstat = fpstat_in;
#if defined(__i386__) || defined(__x86_64__)
  __asm__ volatile ("ldmxcsr %0" : "=m" (fpstat));
#endif
  return ;
}

#if defined(SELF_TEST)
int get_cpu_core_thread()  /* Intel CPUs only and even in this case not always reliable */
{
#if defined(__x86_64__)
  uint32_t regs[4];
  if(ProcessorCapabilities == 0) get_cpu_capabilities();
  X86_cpuid( 0x0B, 0, regs );
  return( ((regs[3]>>regs[0]) << 8) | (regs[3]-((regs[3]>>regs[0])<<regs[0])) );  /* core << 8 + thread  */
#else
  return 0;
#endif
}

int main(int argc, char** argv)
{
  int core_and_thread;
  int status;
  float r;

  printf("CPU with %d cores and %d threads/core \n",get_cpu_cores(),get_cpu_hyperthreads());
  printf("FPU status = %8.8x\n",get_fp_status_ctl());
  core_and_thread = get_cpu_core_thread();
  printf("core = %d, thread = %d\n",core_and_thread>>8,core_and_thread&0xFF);
  printf("process is running on cpu number = %d\n",get_cpu_number());

  printf("CPU speed: %lu Hz\n",get_cpu_freq());
  printf("CPU cycle = %g sec\n",cycle);
  printf("FLAGS: ");
  if(cpu_has_feature(FLAG_SSE))  printf(" SSE");
  if(cpu_has_feature(FLAG_SSE2)) printf(" SSE2");
  if(cpu_has_feature(FLAG_SSE3)) printf(" SSE3");
  if(cpu_has_feature(FLAG_SSE4)) printf(" SSE4");
  if(cpu_has_feature(FLAG_AVX))  printf(" AVX");
  if(cpu_has_feature(FLAG_FMA))  printf(" FMA");
  if(cpu_has_feature(FLAG_AVX2)) printf(" AVX2");
  printf("\n");
  printf("CPU Version string: '%s'\n",cstring);
  status = get_fp_status_ctl();
  printf("FPU status = %8.8x\n",status);
  printf("Precision status %s\n",status & FP_STATUS_PE ? "ON" : "OFF");
  printf("Underflow status %s\n",status & FP_STATUS_UE ? "ON" : "OFF");
  printf("Overflow status %s\n",status & FP_STATUS_OE ? "ON" : "OFF");
  printf("Zero divide status %s\n",status & FP_STATUS_ZE ? "ON" : "OFF");
  printf("Forcing underflow, zero divide, overflow\n");
  r = 1.0E-30; r = r*r; /* underflow */
  if(r > 0.0) r = 0.0;
  r = 1.0/r;            /* zero divide */
  r = 1.0E30; r = r*r;  /* overflow */
  status = get_fp_status_ctl();
  printf("FPU status = %8.8x\n",status);
  printf("Precision status %s\n",status & FP_STATUS_PE ? "ON" : "OFF");
  printf("Underflow status %s\n",status & FP_STATUS_UE ? "ON" : "OFF");
  printf("Overflow status %s\n",status & FP_STATUS_OE ? "ON" : "OFF");
  printf("Zero divide status %s\n",status & FP_STATUS_ZE ? "ON" : "OFF");
  return (0);
}
#endif
