/* Hopefully useful routines for C and FORTRAN
 * Copyright (C) 2020  Recherche en Prevision Numerique
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation,
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include <stdint.h>
#include <stdlib.h>

//****P* librkl/compression-expansion
// Synopsis
//
//  grid aggregation and restoration to original dimensions
//
//   how it works in 1D (in 2D, same method along j axis)
//
//   original grid (even number of points, ni):
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
//   |     |     |     |     |     |     |     |     |     |     |
//   |  1  |  2  |  3  |  4  |  *  |  *  |  *  |  M  |  N  |  L  |
//   |     |     |     |     |     |     |     |     |     |     |
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
//
//   new grid (type a) : ni/2 points
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
//   |           |           |           |           |           | (normal aggregation)
//   |     A     |     B     |     *     |     C     |     D     | all new points are halfway
//   |           |           |           |           |           | between original points
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
//   aggregation :
//    A = average of 1 and 2 (all pairs the same)
//    D = average of N and L
//   restoring :
//    point 1 = 1.25 * A - .25 * B (linear extrapolation)
//    point 2 = .75 * A + .25 * B  ( .75 * previous + .25 * next)
//    point 3 = .25 * A + .75 * B  ( .25 * previous + .75 * next)
//    (same recipe for following pairs, linear interpolation
//    point L = 1.25 * D - .25 * C (linear extrapolation)
//
//   new grid (type b) : ni/2 + 1 points
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
//   |     |           |           |           |           |     | (modified type b aggregation)
//   |  A  |     B     |     *     |     C     |     D     |  E  | first and last points at same position
//   |     |           |           |           |           |     | as in original grid
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
//   aggregation :
//    A = 1
//    B = average of 2 and 3 (all pairs the same)
//    D = average of M and N
//    E = L
//   restoring :
//    point 1 = A
//    point 2 = .25 * A + .75 * B  ( .25 * previous + .75 * next)
//    point 3 = .75 * B + .25 * C  ( .75 * previous + .25 * next)
//    (same recipe for following pairs, linear interpolation
//    point E = L
//    purist point of view : point 2 = 1/3 * A + 2/3 * B  
//    purist point of view : point N = 2/3 * D + 1/3 * E  
//    aggregation of integers : the result is scaled up by a factor 4 to avoid precision losses
//
//========================================================================================
//   original grid (odd number of points, ni):
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
//   |     |     |     |     |     |     |     |     |     |
//   |  1  |  2  |  3  |  4  |  *  |  *  |  K  |  N  |  L  |
//   |     |     |     |     |     |     |     |     |     |
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
//
//   new grid (type a) : (ni+1)/2 points
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
//   |           |           |           |           |     | (modified type a aggregation)
//   |     A     |     B     |     *     |     C     |  D  | last point at same position
//   |           |           |           |           |     | as in original grid
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
//   aggregation :
//    A = average of 1 and 2
//    D = value of L
//   restoring :
//    point 1 = 1.25 * A - .25 * B (linear extrapolation)
//    point 2 = .75 * A + .25 * B  ( .75 * previous + .25 * next)
//    point 3 = .25 * A + .75 * B  ( .25 * previous + .75 * next)
//    (same recipe for following pairs, linear interpolation)
//    point N = C * 2/3 + D * 1/3  (linear interpolation, different coefficients)
//    point L =  D 
//
//   new grid (type b) : (ni+1)/2 + 1 points
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
//   |     |           |           |           |     |     | (modified type b aggregation)
//   |  A  |     B     |     *     |     C     |  D  |  E  | first point and last 2 points
//   |     |           |           |           |     |     | at same position as in original grid
//   +-----+-----+-----+-----+-----+-----+-----+-----+-----+
//   aggregation :
//    A = 1
//    B = .5 * (2 + 3)
//    D = N
//    E = L
//   restoring :
//    point 1 = A
//    point 2 = .25 * A + .75 * B  ( .25 * previous + .75 * next)
//    point 3 = .75 * B + .25 * C  ( .75 * previous + .25 * next)
//    (same recipe for following pairs, linear interpolation
//    point N = D
//    point L = E
//    purist point of view : point 2 = 1/3 * A + 2/3 * B
//    purist point of view : point K = 2/3 * C + 1/3 * D
//    aggregation of integers : the result is scaled up by a factor 4 to avoid precision losses
//****

static inline int packed_dimension(int lunp, int style){
  if(lunp < 2) return -1;                       // not enough points (min 2)
  if(style == 0) return (lunp + 1) / 2;         // type A compression
  if(style == 1) return 1 + ((lunp + 1) / 2);   // type B compression
  if(style == 2) return (lunp + 1) / 2;         // type A compression
  if(style == 3) return 1 + ((lunp + 1) / 2);   // type B compression
  return -1 ;                                   // unrecognized compression
}
// get packed dimension necessary to compress lunp points of uncompressed data
// lunp   : number of points to compress
// style  : compression type (0 = type A, 1 = type B)
// -1 is returned in case of error
int get_compressed_dimension(int lunp, int style)  //InTc
{
  return packed_dimension(lunp, style);
}
//****f* librkl/Get_compressed_dimensions
// Synopsis
//
// get packed dimension necessary to compress a 2D array
// ni, nj    : useful dimensions of array to compress (actual array may be larger)
// nip, njp  : necessary dimensions for compressed array (actual array may be larger)
//
// FORTRAN interface
// interface                                                    !InTf!
//   subroutine Get_compressed_dimensions(nip, njp, ni, nj, style) bind(C,name='Get_compressed_dimensions') !InTf!
//     import :: C_INT                                          !InTf!
//     integer(C_INT), intent(OUT) :: nip, njp                  !InTf!
//     integer(C_INT), intent(IN), value  :: ni, nj, style      !InTf!
//   end subroutine Get_compressed_dimensions                   !InTf!
// end interface                                                !InTf!
// ARGUMENTS
void Get_compressed_dimensions(int *nip, int *njp, int ni, int nj, int style)  //InTc
//****
{
  *nip = packed_dimension(ni, style);
  *njp = packed_dimension(nj, style);
}

//****f* librkl/Compress_by_2A
// Synopsis
//
// use modified aggregation (type a) to reduce array fi(ni, nj) to array fo(lnio, (nj+1)/2)
// fi        : input array (not packed)
// ni, nj    : number of useful points along i and j
// li        : first dimension (Fortran style) of array fi
// fo        : output array (packed)
// lnio, lnjo: dimensions (Fortran style) of array fo
// return packing descriptor if OK, -1 if output array dimensions are improper.
//
// N.B.  lnio must be >= (ni+1)/2, lnjo must be >= (nj+1)/2
//
// FORTRAN interface
// interface                                                          !InTf!
// function Compress_by_2A(fi, ni, nj, lni, fo, lnio, lnjo) result(status) bind(C,name='Compress_by_2A')    !InTf!
//   import :: C_INT, C_FLOAT, C_LONG_LONG                            !InTf!
//   integer(C_INT), intent(IN), value :: ni, nj, lni, lnio, lnjo     !InTf!
//   real(C_FLOAT), intent(IN), dimension(lni,nj)    :: fi            !InTf!
//   real(C_FLOAT), intent(OUT), dimension(lnio,lnjo) :: fo           !InTf!
//   integer(C_LONG_LONG) :: status                                   !InTf!
// end function Compress_by_2A                                        !InTf!
// end interface                                                      !InTf!
// ARGUMENTS
int64_t Compress_by_2A(float *fi, int ni, int nj, int lni, float *fo, int lnio, int lnjo)  //InTc
//****
{
  int i, j, nio, njo;
  float *unp = fi;
  float *pak = fo;
  int64_t result;

  nio = (ni+1)/2;   // number of packed points along i
  njo = (nj+1)/2;   // number of packed points along j
  if(lnio < nio || lnjo < njo) return -1;  // output array dimensions are too small

  for(j = 0 ; j < nj -1 ; j += 2){    // pairs of rows (even/odd rows)
    for(i = 0 ; i < ni/2 ; i++){
      pak[i] = 0.25f * (unp[2*i] + unp[2*i+1] + unp[2*i+lni] + unp[2*i+lni+1]);
    }
    if(ni & 1) {                      // ni is odd, orphan values at end of rows
      pak[ni/2] = 0.5f * (unp[2*i] + unp[2*i+lni]);  // no mirror condition
    }
    pak += lnio;
    unp += 2*lni;
  }
  if(nj & 1){                         // nj is odd, orphan row at top
    for(i = 0 ; i < ni/2 ; i++){
      pak[i] = 0.5f * (unp[2*i] + unp[2*i+1]);
    }
    if(ni & 1) {                      // ni is odd, orphan value at end of row
      pak[ni/2] = unp[2*i];
    }
  }
  result = 0 ; // type A compression
  result = (result << 28) | ni ; result = (result << 28) | nj ;   // compress descriptor
  return result;
}

// for single row at unp
static inline void compress_along_ib1(float *pak, float *unp, int ni){
  int i;
  pak[0] = unp[0];
  for(i = 1 ; i < ni/2 ; i++){
    pak[i] = .5f*(unp[2*i] + unp[2*i-1]);
  }
  pak[(ni+1)/2] = unp[ni-1];
  if(ni & 1) pak[(ni-1)/2] = unp[ni-2];
}

// version of above function for integers. unp is scaled up by a factor of 4.
static inline void compress_along_ib1I(int *pak, int *unp, int ni){
  int i;
  pak[0] = unp[0] << 2;
  for(i = 1 ; i < ni/2 ; i++){
    pak[i] = (unp[2*i] + unp[2*i-1]) << 1;
  }
  pak[(ni+1)/2] = unp[ni-1] << 2;
  if(ni & 1) pak[(ni-1)/2] = unp[ni-2] << 2;
}

// for pair of rows at unp and unp+lni
static inline void compress_along_ib2(float *pak, float *unp, int ni, int lni){
  int i;
  pak[0] = 0.5f*(unp[0] + unp[lni]);
  for(i = 1 ; i < ni/2 ; i++){
    pak[i] = .25f*(unp[2*i] + unp[2*i-1] + unp[2*i+lni] + unp[2*i-1+lni]);
  }
  pak[(ni+1)/2] = 0.5f*(unp[ni-1]+unp[ni-1+lni]);
  if(ni & 1) pak[(ni-1)/2] = 0.5f*(unp[ni-2] + unp[ni-2+lni]);
}

// version of above function for integers. unp is scaled up by a factor of 4.
static inline void compress_along_ib2I(int *pak, int *unp, int ni, int lni){
  int i;
  pak[0] = (unp[0] + unp[lni]) << 1;
  for(i = 1 ; i < ni/2 ; i++){
    pak[i] = (unp[2*i] + unp[2*i-1] + unp[2*i+lni] + unp[2*i-1+lni]);
  }
  pak[(ni+1)/2] = (unp[ni-1]+unp[ni-1+lni]) << 1;
  if(ni & 1) pak[(ni-1)/2] = (unp[ni-2] + unp[ni-2+lni]) << 1;
}

//****f* librkl/Compress_by_2B
// Synopsis
//
// use modified aggregation (type b) to reduce array fi(lni, nj) to array fo(lnio, 1 + (nj+1)/2 )
// fi        : input array (not packed)
// ni, nj    : number of useful points along i and j
// lni       : first dimension (Fortran style) of array fi
// fo        : output array (packed)
// lnio, lnjo: dimensions (Fortran style) of array fo
// return packing descriptor if OK, -1 if output array dimensions are improper.
//
// N.B.  lnio must be >= 1 + (ni+1)/2, njo must be >= 1 + (nj+1)/2
//
// FORTRAN interface
// interface                                                          !InTf!
// function Compress_by_2B(unp, ni, nj, lni, pak, lnio, lnjo) result(status) bind(C,name='Compress_by_2B')    !InTf!
//   import :: C_INT, C_FLOAT, C_LONG_LONG                            !InTf!
//   integer(C_INT), intent(IN), value :: ni, nj, lni, lnio,lnjo      !InTf!
//   real(C_FLOAT), intent(IN), dimension(lni,nj)     :: unp          !InTf!
//   real(C_FLOAT), intent(OUT), dimension(lnio,lnjo) :: pak          !InTf!
//   integer(C_LONG_LONG) :: status                                   !InTf!
// end function Compress_by_2B                                        !InTf!
// end interface                                                      !InTf!
// ARGUMENTS
int64_t Compress_by_2B(float *fi, int ni, int nj, int lni, float *fo, int lnio, int lnjo)  //InTc
//****
{
  int j, nio, njo;
  float *unp = fi;
  float *pak = fo;
  int64_t result;

  nio = (ni+1)/2 + 1 ;
  njo = (nj+1)/2 + 1;
  if(lnio < nio || lnjo < njo) return -1;  // output array dimensions are too small

  compress_along_ib1(pak, unp, ni);   // bottom row
  unp += lni ; pak += lnio;
  for(j = 1 ; j < nj/2 ; j ++){       // pairs of rows
    compress_along_ib2(pak, unp, ni, lni);
    pak += lnio;
    unp += 2*lni;
  }
  compress_along_ib1(pak, unp, ni);   // top row (next to last row if nj is odd)
  unp += lni ; pak += lnio;
  if(nj & 1){                         // nj is odd, row at top
    compress_along_ib1(pak, unp, ni);
  }
  result = 1 ; // type B compression
  result = (result << 28) | ni ; result = (result << 28) | nj ;   // compress descriptor
  return result;
}

// version of above for integers the packed array is scaled up by a factor of 4 to avoid precision losses
int64_t Compress_by_2BI(int *fi, int ni, int nj, int lni, int *fo, int lnio, int lnjo)  //InTc
{
  int j, nio, njo;
  int *unp = fi;
  int *pak = fo;
  int64_t result;

  nio = (ni+1)/2 + 1 ;
  njo = (nj+1)/2 + 1;
  if(lnio < nio || lnjo < njo) return -1;  // output array dimensions are too small

  compress_along_ib1I(pak, unp, ni);   // bottom row
  unp += lni ; pak += lnio;
  for(j = 1 ; j < nj/2 ; j ++){       // pairs of rows
    compress_along_ib2I(pak, unp, ni, lni);
    pak += lnio;
    unp += 2*lni;
  }
  compress_along_ib1I(pak, unp, ni);   // top row (next to last row if nj is odd)
  unp += lni ; pak += lnio;
  if(nj & 1){                         // nj is odd, row at top
    compress_along_ib1I(pak, unp, ni);
  }
  result = 3 ; // type B compression for integers
  result = (result << 28) | ni ; result = (result << 28) | nj ;   // compress descriptor
  return result;
}

//****f* librkl/Compress_by_2
// Synopsis
//
// generic entry Compress_by_2
// uses modified aggregation to compress array unp into array pak
// fi        : input array (not packed)
// ni, nj    : number of useful points along i and j
// lni       : first dimension (Fortran style) of array fi
// fo        : output array (packed)
// lnio, lnjo: dimensions (Fortran style) of array fo
// style     : compression style (0 = type A, 1 = type B)
// return packing descriptor if OK, -1 if output array dimensions are improper.
//
// the following relationships MUST be respected
// type A
//   uncompressed ni => compressed (ni+1)/2
//   uncompressed nj => compressed (nj+1)/2
// type B
//   uncompressed ni => compressed 1+(ni+1)/2
//   uncompressed nj => compressed 1+(nj+1)/2
//
// compression descriptor  style:8 ni:28 nj:28
//
// FORTRAN interface
// interface                                                          !InTf!
// function Compress_by_2(unp, ni, nj, lni, pak, lnio, lnjo, style) result(status) bind(C,name='Compress_by_2')    !InTf!
//   import :: C_INT, C_FLOAT, C_LONG_LONG                            !InTf!
//   integer(C_INT), intent(IN), value :: ni, nj, lni, lnio,lnjo      !InTf!
//   integer(C_INT), intent(IN), value :: style                       !InTf!
//   real(C_FLOAT), intent(IN), dimension(lni,nj)    :: unp           !InTf!
//   real(C_FLOAT), intent(OUT), dimension(lnio,lnjo) :: pak          !InTf!
//   integer(C_LONG_LONG) :: status                                   !InTf!
// end function Compress_by_2                                         !InTf!
// end interface                                                      !InTf!
// ARGUMENTS
int64_t Compress_by_2(void *unp, int ni, int nj, int lni, void *pak, int lnio, int lnjo, int style)  //InTc
//****
{
  int64_t cdesc = -1;
  if(style == 0){
    cdesc = Compress_by_2A((float *)unp, ni, nj, lni, (float *)pak, lnio, lnjo);
  }
  if(style == 1){
    cdesc = Compress_by_2B((float *)unp, ni, nj, lni,(float *) pak, lnio, lnjo);
  }
//   if(style == 2){   // not implemented for the time being
//     cdesc = Compress_by_2AI((int *)unp, ni, nj, lni,(int *) pak, lnio, lnjo);
//   }
  if(style == 3){
    cdesc = Compress_by_2BI((int *)unp, ni, nj, lni,(int *) pak, lnio, lnjo);
  }
  return cdesc;
}

// one dimensional array expansion, 
// nio/2 -> nio      (nio even)
// (nio-1)/2 -> nio  (nio odd)
static inline void expand_along_ia(float *pak, float *unp, int nio){
  int i;
  unp[0] = 1.25f*pak[0] - 0.25f*pak[1];
  for(i = 1; i < (nio/2) ; i++){
    unp[2*i-1] = .75f*pak[i-1] + .25f*pak[i];
    unp[2*i  ] = .25f*pak[i-1] + .75f*pak[i];
  }
  if(nio & 1){
    unp[2*i-1] = .6666667f*pak[i-1] + .3333333f*pak[i];  // different weights for next to last point if nio odd
    unp[2*i  ] = pak[i];                               // last point is at the right coordinates
  }else{
    unp[2*i-1] = 1.25f*pak[i-1] - .25f*pak[i-2];           // even number of points, linear extrapolation
  }
}

//****f* librkl/Expand_by_2A
// Synopsis
//
// restore array fo from previously aggregated (type a) array fi (see Compress_by_2)
// fi        : input array (packed)
// ni, nj    : number of useful points along i and j for array fi
// lni       : first dimension (Fortran style) of array fi
// fo        : output array (unpacked)
// nio, njo  : number of useful points along i and j for array fo
// lnio      : first dimension (Fortran style) of array fo
// return value is 0 if OK, -1 if output array dimensions are too small.
//
// nio MUST BE >= 2*ni -1 , njo MUST BE >= 2*nj-1
// if nio > 2*ni, a useful dimension of 2*ni will be used
// if njo > 2*nj, a useful dimension of 2*nj will be used
// return value is > 0 if OK, -1 if output array dimensions are too small.
// FORTRAN interface
// interface                                                          !InTf!
// function Expand_by_2A(fi, ni, nj, lni, fo, nio, njo, lnio) result(status) bind(C,name='Expand_by_2A')    !InTf!
//   import :: C_INT, C_FLOAT, C_LONG_LONG                            !InTf!
//   integer(C_INT), intent(IN), value :: ni, nj, lni, nio, njo, lnio !InTf!
//   real(C_FLOAT), intent(IN), dimension(lni,nj)    :: fi            !InTf!
//   real(C_FLOAT), intent(OUT), dimension(lnio,njo) :: fo            !InTf!
//   integer(C_LONG_LONG) :: status                                   !InTf!
// end function Expand_by_2A                                          !InTf!
// end interface                                                      !InTf!
// ARGUMENTS
int64_t Expand_by_2A(float *fi, int ni, int nj, int lni, float *fo, int nio, int njo, int lnio)  //InTc
//****
{
  int i, j;
  float *pak = fi;
  float *unp = fo;
  float t[ni];    // one row of input array
  int64_t result;

  if(nio < 2*ni -1 || njo < 2*nj-1 || lnio < nio) return -1; // output array too small
  nio = (nio >= 2*ni) ? 2*ni : nio;
  njo = (njo >= 2*nj) ? 2*nj : njo;

  for(i = 0 ; i < ni ; i++){                  // first row, extrapolation along j
    t[i] = 1.25f*pak[i] - .25f*pak[i+lni];
  }
  expand_along_ia(t, unp, nio);
  for(j = 1 ; j < (njo/2) ; j++){             // pairs of output rows 
    for(i = 0 ; i < ni ; i++){                // lower row, interpolation along j
      t[i] = .75f*pak[i] + .25f*pak[i+lni];
    }
    unp += lnio;
    expand_along_ia(t, unp, nio);              // store expanded lower row
    for(i = 0 ; i < ni ; i++){                // upper row, interpolation along j
      t[i] = .25f*pak[i] + .75f*pak[i+lni];
    }
    unp += lnio;
    expand_along_ia(t, unp, nio);              // store expanded upper row
    pak += lni;
  }
  if(njo & 1){                                // nio is odd, last 2 rows are different
    for(i = 0 ; i < ni ; i++){                // next to last row, different weights
      t[i] = .6666667f*pak[i] + .3333333f*pak[i+lni];
    }
    unp += lnio;
    expand_along_ia(t, unp, nio);
    for(i = 0 ; i < ni ; i++){                // last row as is
      t[i] = pak[i+lni];
    }
    unp += lnio;
    expand_along_ia(t, unp, nio);              // store expanded last row
  }else{                                      // njo is even, last row, extrapolation along j
    for(i = 0 ; i < ni ; i++){
      t[i] = 1.25f*pak[i] - .25f*pak[i-lni];
    }
    unp += lnio;
    expand_along_ia(t, unp, nio);              // store expanded last row
  }
  result = nio ; result = (result << 32) | njo ;   // (nio << 32) | njo
  return result;
}

// only expand middle pairs and copy first element. this is usable ONLY by Expand_by_2B
// because the operation is performed IN PLACE, the packed array is stored at the tail 
// of the output unpacked array
static inline void expand_along_ib(float *pak, float *unp, int nio){
  register int i;
  register float corr, d1, d2, tl, th, tm;
  unp[0] = pak[0] ;
  tl = pak[0]; tm = pak[1];
  for(i = 1; i < (nio/2) ; i++){      // tm = pak[i], tl = pak[i-1], th = pak[i+1];
    th = pak[i+1];
    d1 = .25f*tl + .75f*tm;
    d2 = .25f*th + .75f*tm;
    corr = tm - (d1 + d2) * .5f;      // error on average. pak[i] should be average(d1 , d2)
    unp[2*i-1] = d1 + corr;             // apply correction
    unp[2*i  ] = d2 + corr;
    tl = tm ; tm = th ;
  }
}

// version of above function for integers (preliminary)
static inline void expand_along_ibI(int *pak, int *unp, int nio){
  register int i;
  register int corr, d1, d2, tl, th, tm;
  unp[0] = (pak[0] + 2) >> 2;
  tl = pak[0]; tm = pak[1];
  for(i = 1; i < (nio/2) ; i++){        // tm = pak[i], tl = pak[i-1], th = pak[i+1];
    th = pak[i+1];
    d1 = (tl + tm + tm + tm + 2) >> 2 ; // +2 for rounding
    d2 = (th + tm + tm + tm + 2) >> 2;  // +2 for rounding
    corr = tm - ((d1 + d2) >> 1) ;        //  error on average. pak[i] should be average(d1 , d2)
    unp[2*i-1] = (d1 + corr + 2) >> 2;    // apply correction with rounding
    unp[2*i  ] = (d2 + corr + 2) >> 2;
    tl = tm ; tm = th ;
  }
  for(i = 2*i -1 ; i < nio ; i++) unp[i] = (unp[i] + 2) >> 2;
}

//****f* librkl/Expand_by_2B
// Synopsis
//
// restore array fo from previously aggregated (type a) array fi (see Compress_by_2)
// fi        : input array
// ni, nj    : number of useful points along i and j for array fi
// lni       : first dimension (Fortran style) of array fi
// fo        : output array
// nio, njo  : number of useful points along i and j for array fo
// lnio      : first dimension (Fortran style) of array fo
// return value is 0 if OK, -1 if output array dimensions are improper.
//
// the following relationships MUST be respected
// (nio+1)/2+1 == ni  lnio >= nio
// (njo+1)/2+1 == nj
//
// return value is > 0 if OK, -1 if output array dimensions are improper.
// FORTRAN interface
// interface                                                          !InTf!
// function Expand_by_2B(fi, ni, nj, lni, fo, nio, njo, lnio) result(status) bind(C,name='Expand_by_2B')    !InTf!
//   import :: C_INT, C_FLOAT, C_LONG_LONG                            !InTf!
//   integer(C_INT), intent(IN), value :: ni, nj, lni, nio, njo, lnio !InTf!
//   real(C_FLOAT), intent(IN), dimension(lni,nj)    :: fi            !InTf!
//   real(C_FLOAT), intent(OUT), dimension(lnio,njo) :: fo            !InTf!
//   integer(C_LONG_LONG) :: status                                   !InTf!
// end function Expand_by_2B                                          !InTf!
// end interface                                                      !InTf!
// ARGUMENTS
int64_t Expand_by_2B(float *fi, int ni, int nj, int lni, float *fo, int nio, int njo, int lnio)  //InTc
//****
{
  int i, j, offset;
  float *pak = fi;   // packed array
  float *unp = fo;   // unpacked array
  int64_t result;
  float *t1, *t2;
  float corr, d1, d2;

  if((nio+1)/2+1 != ni || (njo+1)/2+1 != nj) return -1; // output array has the wrong dimensions
  offset = nio - ni;
  t1 = unp + offset;    // use upper part of line to store packed version
  for(i = 0 ; i < ni ; i++) t1[i] = pak[i];  // first row
  expand_along_ib(t1, unp, nio);
  unp += lnio ; pak += lni ;

  for(j = 1 ; j < njo/2 ; j++) {            // middle pairs
    t1 = unp + offset; t2 = t1 + lnio;
    for(i = 0 ; i < ni ; i++){
      d1 = .25f*pak[i-lni] + .75f*pak[i];
      d2 = .25f*pak[i+lni] + .75f*pak[i];
      corr = pak[i] - (d1 + d2) * .5f;      // error on average
      t1[i] = d1 + corr;                    // apply correction
      t2[i] = d2 + corr;
    }
    expand_along_ib(t1, unp, nio);          // expansion along i
    unp += lnio ;
    expand_along_ib(t2, unp, nio);          // expansion along i
    unp += lnio ;  pak += lni ;
  }
  t1 = unp + offset;
  for(i = 0 ; i < ni ; i++) t1[i] = pak[i]; // upper row
  expand_along_ib(t1, unp, nio);            // expansion along i
  if(njo & 1) {                             // next to last row if number of rows is odd
    unp += lnio ; pak += lni ;
    t1 = unp + offset;
    for(i = 0 ; i < ni ; i++) t1[i] = pak[i];
    expand_along_ib(t1, unp, nio);
  }
  result = nio ; result = (result << 32) | njo ;   // (nio << 32) | njo
  return result;
}

// packed input values are expected to be 4 times too large
int64_t Expand_by_2BI(int *fi, int ni, int nj, int lni, int *fo, int nio, int njo, int lnio)  //InTc
{
  int i, j, offset;
  int *pak = fi;   // packed array
  int *unp = fo;   // unpacked array
  int64_t result = -1;
  int *t1, *t2;
  int corr, d1, d2;

  if((nio+1)/2+1 != ni || (njo+1)/2+1 != nj) return result ; // output array has insufficient dimensions
  offset = nio - ni;
  t1 = unp + offset;    // use upper part of line to store packed version
  for(i = 0 ; i < ni ; i++) t1[i] = pak[i];  // first row
  expand_along_ibI(t1, unp, nio);
  unp += lnio ; pak += lni ;

  for(j = 1 ; j < njo/2 ; j++) {            // middle pairs
    t1 = unp + offset; t2 = t1 + lnio;
    for(i = 0 ; i < ni ; i++){
      d1 = (pak[i-lni] + pak[i] + pak[i] + pak[i] + 2) >> 2 ; // +2 for rounding
      d2 = (pak[i+lni] + pak[i] + pak[i] + pak[i] + 2) >> 2 ; // +2 for rounding
      corr = pak[i] - ((d1 + d2) >> 1);      // error on average
      t1[i] = d1 + corr;                    // apply correction
      t2[i] = d2 + corr;
    }
    expand_along_ibI(t1, unp, nio);          // expansion along i
    unp += lnio ;
    expand_along_ibI(t2, unp, nio);          // expansion along i
    unp += lnio ;  pak += lni ;
  }
  t1 = unp + offset;
  for(i = 0 ; i < ni ; i++) t1[i] = pak[i]; // upper row
  expand_along_ibI(t1, unp, nio);            // expansion along i
  if(njo & 1) {                             // next to last row if number of rows is odd
    unp += lnio ; pak += lni ;
    t1 = unp + offset;
    for(i = 0 ; i < ni ; i++) t1[i] = pak[i];
    expand_along_ibI(t1, unp, nio);
  }
  result = nio ; result = (result << 32) | njo ;   // (nio << 32) | njo
  return result;
}

//****f* librkl/Expand_by_2
// Synopsis
//
// restore array fo from previously aggregated (type a) array fi (see Compress_by_2)
// pak       : input array (packed)
// lnip, lnjp: dimensions (Fortran style) of array pak
// unp       : output array (unpacked)
// lni, lnj  : dimensions (Fortran style) of array unp
// cdesc     : compression descriptor from Compress_by_2 style:8 ni:28 nj:28
//
// return value is 0 if OK, -1 if output array dimensions are improper.
// lni and lnj must be large enough to contain the unpacked result
// lnip and lnjp are checked for compatibility with cdesc information
//
// the following relationships MUST be respected
// type A
//   uncompressed ni => compressed (ni+1)/2
//   uncompressed nj => compressed (nj+1)/2
// type B
//   uncompressed ni => compressed 1+(ni+1)/2
//   uncompressed nj => compressed 1+(nj+1)/2
//
// return value is > 0  if OK, -1 if output array dimensions are improper.
// FORTRAN interface
// interface                                                          !InTf!
// function Expand_by_2(pak, lnip, lnjp, unp, lni, lnj, cdesc) result(status) bind(C,name='Expand_by_2')    !InTf!
//   import :: C_INT, C_FLOAT, C_LONG_LONG                            !InTf!
//   integer(C_INT), intent(IN), value :: lnip, lnjp, lni, lnj        !InTf!
//   integer(C_LONG_LONG), intent(IN), value :: cdesc                 !InTf!
//   real(C_FLOAT), intent(IN), dimension(lnip,lnjp) :: pak           !InTf!
//   real(C_FLOAT), intent(OUT), dimension(lni,lnj)  :: unp           !InTf!
//   integer(C_LONG_LONG) :: status                                   !InTf!
// end function Expand_by_2                                           !InTf!
// end interface                                                      !InTf!
// ARGUMENTS
int64_t Expand_by_2(void *pak, int lnip, int lnjp, void *unp, int lni, int lnj, int64_t cdesc)  //InTc
//****
{
  int style = cdesc >> 56;   // upper 8 bits of cdesc
  int64_t status = -1;
  int nipak, njpak, ni, nj;
  ni = (cdesc >> 28) & 0xFFFFFFF;
  nj = cdesc & 0xFFFFFFF;
  if(lni < ni || lnj < nj) return status ;               // output array too small
  Get_compressed_dimensions(&nipak, &njpak, ni, nj, style);
  if(lnip < nipak || lnjp < njpak) return status ;   // input array too small

  if(style == 0){
    status = Expand_by_2A((float *)pak, nipak, njpak, lnip, (float *)unp, ni, nj, lni);
  }
  if(style == 1){
    status = Expand_by_2B((float *)pak, nipak, njpak, lnip, (float *)unp, ni, nj, lni);
  }
//   if(style == 2){    // not implemented for the time being
//     status = Expand_by_2AI((float *)pak, nipak, njpak, lnip, (float *)unp, ni, nj, lni);
//   }
  if(style == 3){
    status = Expand_by_2BI((int *)pak, nipak, njpak, lnip, (int *)unp, ni, nj, lni);
  }
  return status;
}

#if defined(SELF_TEST)
#include <stdio.h>
#define LNI 15
#define NI 7
#define NIO 10
#define LNJ 12
#define NJ 6
#define NJO 10
#define OFFSET 10
#define FORMULA i*i + j*j + 2.0f*i*j + OFFSET

#define STYLE 1
#define Compress_by_2X Compress_by_2B
#define Expand_by_2X Expand_by_2B

static void analyze_error_i(int *f1, int *f2, int ni, int nj, int lni1, int lni2){
  int i, j;
  float errabs, errabsmax, errabsavg, erravg, avgf1, avgf2;
//   float errrelmax, errrelav;
  errabsmax = 0.0f;
  errabsavg = 0.0f;
  erravg = 0.0f;
  avgf1 = 0.0f;
  avgf2 = 0.0f;
//   errrelmax = 0.0f;
//   errrelavg = 0.0f;
  for(j = 0 ; j < nj ; j++){
    for(i = 0 ; i < ni ; i++){
      avgf1 += f1[i];
      avgf2 += f2[i];
      errabs = f1[i] - f2[i];
      erravg += errabs;
      errabs = (errabs < 0) ? -errabs : errabs;
      errabsavg += errabs;
      errabsmax = (errabsmax < errabs) ? errabs : errabsmax;
    }
    f1 += lni1;
    f2 += lni2;
  }
  errabsavg = errabsavg / (ni*nj);
  erravg = erravg  / (ni*nj);
  avgf1 = avgf1 / (ni*nj); avgf2 = avgf2 / (ni*nj); 
  printf("(int)   max abs error = %8.3f, avg abs error = %8.3f, bias = %8.3f, avg = %8.3f -> %8.3f, n = %d\n",
	 errabsmax,errabsavg,erravg,avgf2,avgf1,ni*nj);
}

static void analyze_error(float *f1, float *f2, int ni, int nj, int lni1, int lni2){
  int i, j;
  float errabs, errabsmax, errabsavg, erravg, avgf1, avgf2;
//   float errrelmax, errrelav;
  errabsmax = 0.0f;
  errabsavg = 0.0f;
  erravg = 0.0f;
  avgf1 = 0.0f;
  avgf2 = 0.0f;
//   errrelmax = 0.0f;
//   errrelavg = 0.0f;
  for(j = 0 ; j < nj ; j++){
    for(i = 0 ; i < ni ; i++){
      avgf1 += f1[i];
      avgf2 += f2[i];
      errabs = f1[i] - f2[i];
      erravg += errabs;
      errabs = (errabs < 0) ? -errabs : errabs;
      errabsavg += errabs;
      errabsmax = (errabsmax < errabs) ? errabs : errabsmax;
    }
    f1 += lni1;
    f2 += lni2;
  }
  errabsavg = errabsavg / (ni*nj);
  erravg = erravg  / (ni*nj);
  avgf1 = avgf1 / (ni*nj); avgf2 = avgf2 / (ni*nj); 
  printf("(float) max abs error = %8.3f, avg abs error = %8.3f, bias = %8.3f, avg = %8.3f -> %8.3f, n = %d\n",
	 errabsmax,errabsavg,erravg,avgf2,avgf1,ni*nj);
}

int main(int argc, char **argv){
  float fie[LNI*LNJ], foe[NIO*NJO], fir[LNI*LNJ];
  int iie[LNI*LNJ], ioe[NIO*NJO], iir[LNI*LNJ];
  int i, j, nio, njo;
  int64_t status, cdesc, cdesc2;

  printf("========= (%d x %d) ===========\n",NI,NJ);
  for(i=0 ; i<LNI*LNJ ; i++) fie[i] = 0.0f;
  for(j = NJ-1 ; j >= 0 ; j--){
    for(i=0 ; i<NI ; i++){
      fie[i + LNI*j] = FORMULA;
      fir[i + LNI*j] = fie[i + LNI*j];
      iir[i + LNI*j] = fie[i + LNI*j];
      iie[i + LNI*j] = fie[i + LNI*j];
      printf(" %8.3f",fie[i + LNI*j]);
    }
    printf("\n");
  }
  printf("========== compressed ==========\n");
  cdesc = Compress_by_2(fie, NI, NJ, LNI, foe, NIO, NJO, STYLE);
  cdesc2 = Compress_by_2(iie, NI, NJ, LNI, ioe, NIO, NJO, 3);
  Get_compressed_dimensions(&nio, &njo, NI, NJ, STYLE);
  if(cdesc < 0) printf("ERROR in compress: output array too small\n");
  for(j = njo-1 ; j >= 0 ; j--){
    for(i=0 ; i<nio ; i++){
      printf(" %8.3f",foe[i + NIO*j]);
    }
    printf(" | ");
    for(i=0 ; i<nio ; i++){
      printf(" %8d",ioe[i + NIO*j]);
    }
    printf("\n");
  }
  printf("========== restored ==========\n");
  for(i=0 ; i<LNI*LNJ ; i++){ fie[i] = 0.0f; iie[i] = 0 ; }
  status = Expand_by_2(foe, NIO, NJO, fie, LNI, LNJ, cdesc);
  if(status < 0) printf("ERROR in expand: output array too small\n");
  status = Expand_by_2(ioe, NIO, NJO, iie, LNI, LNJ, cdesc2);
  if(status < 0) printf("ERROR in expandI: output array too small\n");
  for(j = NJ-1 ; j >= 0 ; j--){
    for(i=0 ; i<NI ; i++){
      printf(" %8.3f",fie[i + LNI*j]);
    }
    printf(" | ");
    for(i=0 ; i<NI ; i++){
      printf(" %8d",iie[i + LNI*j]);
    }
    printf("\n");
  }
  analyze_error(fie, fir, NI, NJ, LNI, LNI);
  analyze_error_i(iie, iir, NI, NJ, LNI, LNI);
  printf("========= (%d x %d) ===========\n",NI-1,NJ-1);
  for(i=0 ; i<LNI*LNJ ; i++) fie[i] = 0.0f;
  for(j = NJ-2 ; j >= 0 ; j--){
    for(i=0 ; i<NI-1 ; i++){
      fie[i + LNI*j] = FORMULA;
      fir[i + LNI*j] = fie[i + LNI*j];
      printf(" %8.3f",fie[i + LNI*j]);
    }
    printf("\n");
  }
  printf("========== compressed ==========\n");
  cdesc = Compress_by_2(fie, NI-1, NJ-1, LNI, foe, NIO, NJO, STYLE);
  Get_compressed_dimensions(&nio, &njo, NI-1, NJ-1, STYLE);
  if(cdesc < 0) printf("ERROR in compress: output array too small\n");
  for(j = njo-1 ; j >= 0 ; j--){
    for(i=0 ; i<nio ; i++){
      printf(" %8.3f",foe[i + NIO*j]);
    }
    printf("\n");
  }
  printf("========== restored ==========\n");
  for(i=0 ; i<LNI*LNJ ; i++) fie[i] = 0.0f;
  status = Expand_by_2(foe, NIO, NJO, fie, LNI, LNJ, cdesc);
  if(status < 0) printf("ERROR in expand: output array too small\n");
  for(j = NJ-2 ; j >= 0 ; j--){
    for(i=0 ; i<NI-1 ; i++){
      printf(" %8.3f",fie[i + LNI*j]);
    }
    printf("\n");
  }
  analyze_error(fie, fir, NI-1, NJ-1, LNI, LNI);
  printf("========= (%d x %d) ===========\n",NI-1,NJ);
  for(i=0 ; i<LNI*LNJ ; i++) fie[i] = 0.0f;
  for(j = NJ-1 ; j >= 0 ; j--){
    for(i=0 ; i<NI-1 ; i++){
      fie[i + LNI*j] = FORMULA;
      fir[i + LNI*j] = fie[i + LNI*j];
      printf(" %8.3f",fie[i + LNI*j]);
    }
    printf("\n");
  }
  printf("========== compressed ==========\n");
  cdesc = Compress_by_2(fie, NI-1, NJ, LNI, foe, NIO, NJO, STYLE);
  Get_compressed_dimensions(&nio, &njo, NI-1, NJ, STYLE);
  if(cdesc < 0) printf("ERROR in compress: output array too small\n");
  for(j = njo-1 ; j >= 0 ; j--){
    for(i=0 ; i<nio ; i++){
      printf(" %8.3f",foe[i + NIO*j]);
    }
    printf("\n");
  }
  printf("========== restored ==========\n");
  for(i=0 ; i<LNI*LNJ ; i++) fie[i] = 0.0f;
  status = Expand_by_2(foe, NIO, NJO, fie, LNI, LNJ, cdesc);
  if(status < 0) printf("ERROR in expand: output array too small\n");
  for(j = NJ-1 ; j >= 0 ; j--){
    for(i=0 ; i<NI-1 ; i++){
      printf(" %8.3f",fie[i + LNI*j]);
    }
    printf("\n");
  }
  analyze_error(fie, fir, NI-1, NJ, LNI, LNI);
  return 0;
}
#endif
