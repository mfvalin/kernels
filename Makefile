RM = rm

CC = cc

SHELL = /bin/bash

#general optimization level
OPT = -O3

#gcc / clang
CCFLAGS = -mfma -mavx2 
#Intel
#CCFLAGS = -march=core-avx2

SOURCES = fast_endian.c rkl_timers.c cpu_properties.c cores_and_sockets.c avg_expand.c

OBJECTS = fast_endian.o rkl_timers.o cpu_properties.o cores_and_sockets.o avg_expand.o

INCLUDES = fast_endian.h cpu_properties.h cores_and_sockets.inc

LIBRARIES = librkl.a librkl_simd.a

libraries: $(LIBRARIES)

interfaces : avg_expand.inc cores_and_sockets.inc avg_expand.h

cores_and_sockets.inc : cores_and_sockets.c
	grep InTf cores_and_sockets.c | sed -e 's:^.*//::' -e 's/[ ]*!InTf!//' >cores_and_sockets.inc

avg_expand.h: avg_expand.c
	grep InTc avg_expand.c | sed -e 's://.*::' -e 's/$$/;/'  >avg_expand.h

avg_expand.inc: avg_expand.c
	grep InTf avg_expand.c | sed -e 's:^.*//::' -e 's/[ ]*!InTf!//' >avg_expand.inc

dist:	libraries
	[[ -d ../lib ]] && mv $(LIBRARIES) ../lib
	[[ -d ../include ]] && cp $(INCLUDES) ../include

librkl.a: $(SOURCES) $(INCLUDES)
	rm -f $(OBJECTS)
	$(CC) $(OPT) $(CCFLAGS) -DNO_SIMD -I. -c $(SOURCES)
	ar rcv librkl.a $(OBJECTS)
	rm -f $(OBJECTS)

librkl_simd.a: $(SOURCES) $(INCLUDES)
	rm -f $(OBJECTS)
	$(CC) $(OPT) $(CCFLAGS) -DWITH_SIMD -I. -c $(SOURCES)
	ar rcv librkl_simd.a $(OBJECTS)
	rm -f $(OBJECTS)

timer_test:
	$(CC) $(OPT) $(CCFLAGS) -DSELF_TEST -I. rkl_timers.c -o timer_test
	./timer_test
	$(RM) ./timer_test

properties_test: cpu_properties.c
	$(CC) $(CCFLAGS) -DSELF_TEST -I. cpu_properties.c -o properties_test
	./properties_test
	$(RM) ./properties_test

clean:
	rm -f *.o *.s *.a timer_test properties_test a.out
